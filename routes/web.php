<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'IndexController@index')->name('index');

Route::post('/tournaments/generate', 'ResultsController@generateResults')->name('tournaments.generate-results');
Route::post('/tournaments/generate-eliminations', 'ResultsController@generateEliminationTable')->name('tournaments.generate-eliminations');

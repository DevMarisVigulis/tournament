<?php

use App\Models\League;
use App\Models\Team;
use Illuminate\Database\Seeder;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        League::all()->each(static function (League $league) {
            factory(Team::class, League::TEAMS_PER_LEAGUE)->create([
                'league_id' => $league->id
            ]);
        });
    }
}

<?php

use App\Models\League;
use Illuminate\Database\Seeder;

class LeagueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        League::insert(
            [
                [
                    'name' => 'Division A',
                ],
                [
                    'name' => 'Division B',
                ],
            ]
        );

    }
}

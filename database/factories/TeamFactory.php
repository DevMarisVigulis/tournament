<?php

use App\Model;
use App\Models\Team;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/** @var Factory $factory */
$factory->define(Team::class, static function (Faker $faker, $params) {
    static $counter = 1;
    return [
        'name' => 'Team ' . $counter++,
        'league_id' => $params['league_id'],
    ];
});


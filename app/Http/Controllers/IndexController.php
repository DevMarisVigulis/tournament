<?php

namespace App\Http\Controllers;

use App\Models\{League, Result};

class IndexController extends Controller
{
    public function index()
    {
        return view('index.index')->with([
            'leagues' => League::with(['teams', 'teams.games'])->get(),
            'eliminations' => Result::with(['winner', 'loser'])->get(),
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use App\Generators\EliminationTableGenerator;
use App\Generators\TournamentTableGenerator;
use App\Models\{Game, League, Result};
use Exception;
use Illuminate\Http\Request;

class ResultsController extends Controller
{
    /**
     * @param Request                  $request
     * @param TournamentTableGenerator $generator
     * @throws Exception
     */
    public function generateResults(Request $request, TournamentTableGenerator $generator): void
    {
        $leagueId = $request->post('leagueId');
        $leagueTeams = League::find($leagueId)->teams;

        $results = $generator->generate($leagueTeams);

        Game::saveResults($results->toArray(), $leagueId);
    }

    /**
     * @param League                    $league
     * @param EliminationTableGenerator $generator
     * @throws Exception
     */
    public function generateEliminationTable(League $league, EliminationTableGenerator $generator): void
    {
        $teams = $league->getBestAndWorstTeams();

        $results = $generator->generate($teams);

        Result::saveResults($results->toArray());
    }
}

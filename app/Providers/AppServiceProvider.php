<?php

namespace App\Providers;

use App\Http\Controllers\ResultsController;
use App\Generators\{GeneratorInterface, TournamentTableGenerator};
use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(IdeHelperServiceProvider::class);
        }

        $this->app->bind(
            GeneratorInterface::class,
            TournamentTableGenerator::class
        );

        $this->app->when(ResultsController::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

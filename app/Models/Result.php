<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\{Builder, Model, Relations\HasOne};
use Illuminate\Support\Facades\DB;

/**
 * App\Models\Result
 *
 * @method static Builder|Result newModelQuery()
 * @method static Builder|Result newQuery()
 * @method static Builder|Result query()
 * @mixin Eloquent
 */
class Result extends Model
{
    public $table = 'results';
    public $timestamps = false;

    /**
     * @return HasOne
     */
    public function winner(): HasOne
    {
        return $this->hasOne(Team::class, 'id', 'winner_id');
    }

    /**
     * @return HasOne
     */
    public function loser(): HasOne
    {
        return $this->hasOne(Team::class, 'id', 'loser_id');
    }

    /**
     * @param array $results
     */
    public static function saveResults(array $results): void
    {
        DB::transaction(static function () use ($results) {
            foreach ($results as $result) {
                self::insert($result);
            }

        });
    }
}

<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\{Builder, Collection, Model, Relations\BelongsTo, Relations\HasMany};

/**
 * @property int                    $id
 * @property string                 $name
 * @property int                    $league_id
 * @property-read League            $league
 * @method static Builder|Team newModelQuery()
 * @method static Builder|Team newQuery()
 * @method static Builder|Team query()
 * @method static Builder|Team whereId($value)
 * @method static Builder|Team whereLeagueId($value)
 * @method static Builder|Team whereName($value)
 * @mixin Eloquent
 * @property-read Collection|Game[] $games
 * @property-read int|null          $games_count
 * @property-read int               $score
 * @property-read Collection|Game[] $winedGames
 * @property-read int|null          $wined_games_count
 */
class Team extends Model
{
    public $timestamps = false;
    protected $table = 'teams';
    protected $fillable = ['name', 'league_id'];
    protected $casts = [
        'league_id' => 'int',
    ];

    /**
     * @return BelongsTo
     */
    public function league(): BelongsTo
    {
        return $this->belongsTo(League::class, 'id', 'league_id');
    }

    /**
     * @return HasMany
     */
    public function winedGames(): HasMany
    {
        return $this->hasMany(Game::class, 'winner_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function games(): HasMany
    {
        return $this->hasMany(Game::class, 'first_team', 'id');
    }

    /**
     * @return int
     */
    public function getScoreAttribute(): int
    {
        return $this->games->sum('first_team_score');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\{Builder, Model, Relations\HasMany};
use Illuminate\Support\Facades\DB;

/**
 * @property int $id
 * @property int $first_team
 * @property int $second_team
 * @property int $first_team_score
 * @property int $second_team_score
 * @property int $winner_id
 * @method static Builder|Game newModelQuery()
 * @method static Builder|Game newQuery()
 * @method static Builder|Game query()
 * @method static Builder|Game whereFirstTeam($value)
 * @method static Builder|Game whereFirstTeamScore($value)
 * @method static Builder|Game whereId($value)
 * @method static Builder|Game whereSecondTeam($value)
 * @method static Builder|Game whereSecondTeamScore($value)
 * @method static Builder|Game whereWinnerId($value)
 */
class Game extends Model
{
    public $timestamps = false;

    protected $table = 'games';
    protected $fillable = ['first_team', 'second_team', 'first_team_score', 'second_team_score', 'winner_id'];

    /**
     * @param array $results
     * @param int   $leagueId
     * @return void
     */
    public static function saveResults(array $results, int $leagueId): void
    {
        DB::transaction(static function () use ($results, $leagueId) {
            League::find($leagueId)->games()->delete();
            self::insert($results);
        });
    }

    /**
     * @return HasMany
     */
    public function winner(): HasMany
    {
        return $this->hasMany(Team::class, 'id', 'winner_id');
    }

    /**
     * @return HasMany
     */
    public function firstTeam(): HasMany
    {
        return $this->belongsTo(Team::class, 'id', 'first_team');
    }

    /**
     * @return HasMany
     */
    public function secondTeam(): HasMany
    {
        return $this->belongsTo(Team::class, 'id', 'second_team');
    }

    /**
     * @return string
     */
    public function getResultAttribute(): string
    {
        return $this->first_team_score . ' : ' . $this->second_team_score;
    }
}

<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\{Builder, Collection, Model, Relations\HasMany, Relations\HasManyThrough};

/**
 * App\Models\League
 *
 * @property int                    $id
 * @property string                 $name
 * @property-read Collection|Team[] $teams
 * @property-read int|null          $teams_count
 * @method static Builder|League newModelQuery()
 * @method static Builder|League newQuery()
 * @method static Builder|League query()
 * @method static Builder|League whereId($value)
 * @method static Builder|League whereName($value)
 * @mixin Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Game[] $games
 * @property-read int|null $games_count
 */
class League extends Model
{
    public const TEAMS_PER_LEAGUE = 8;

    public $timestamps = false;
    protected $fillable = ['name'];
    protected $table = 'leagues';

    /**
     * @return HasMany
     */
    public function teams(): HasMany
    {
        return $this->hasMany(Team::class, 'league_id', 'id')->orderBy('id');
    }

    /**
     * @return HasManyThrough
     */
    public function games(): HasManyThrough
    {
        return $this->hasManyThrough(Game::class, Team::class, 'league_id', 'first_team', 'id', 'id');
    }

    /**
     * Group teams by leagues
     * Sum each team results
     * Take only best 4 and worst 4
     *
     * @return Collection
     */
    public function getBestAndWorstTeams(): Collection
    {
        $leagues = self::with('games')->get();
        $scores = [];
        $topTeams = new Collection();

            $leagues->each(static function (League $league, $key) use (&$topTeams, &$scores) {

            $gamesByTeams = $league->games->groupBy('first_team');

            $gamesByTeams->each(static function ($games) use (&$scores) {
                $scores[$games->first()->first_team] = $games->sum('first_team_score');
            });

            uasort($scores, static function ($first, $second) use ($key) {
                return $key === 0 ? ($second - $first) : ($first - $second );
            });
            $scores = array_slice($scores, 0, 4, true);
            $topTeams[$league->id] = $scores;
        });
        return $topTeams;
    }
}

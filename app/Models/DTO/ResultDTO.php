<?php

namespace App\Models\DTO;

class ResultDTO extends GameDTO
{
    /**
     * @var int
     */
    private $winnerId;

    /**
     * @var int
     */
    private $loserId;

    /**
     * @param mixed $loserId
     * @return ResultDTO
     */
    public function setLoserId($loserId): ResultDTO
    {
        $this->loserId = $loserId;
        return $this;
    }

    /**
     * @param mixed $winnerId
     * @return ResultDTO
     */
    public function setWinnerId($winnerId): ResultDTO
    {
        $this->winnerId = $winnerId;
        return $this;
    }

    /**
     * @return $this|ResultDTO|mixed
     */
    public function getWinner()
    {
        $this->winnerId = $this->firstTeamScore > $this->secondTeamScore ? $this->firstTeamId : $this->secondTeamId;
        $this->loserId = $this->firstTeamScore < $this->secondTeamScore ? $this->firstTeamId : $this->secondTeamId;

        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'winner_id' => $this->winnerId,
            'loser_id'  => $this->loserId
        ];
    }
}

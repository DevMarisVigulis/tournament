<?php

namespace App\Models\DTO;

use App\Models\Exceptions\WrongTeamException;

class GameDTO
{
    /**
     * @var int
     */
    protected $firstTeamId;

    /**
     * @var int
     */
    protected $secondTeamId;

    /**
     * @var int
     */
    protected $firstTeamScore;

    /**
     * @var int
     */
    protected $secondTeamScore;

    /**
     * @var int
     */
    private $winnerId;

    /**
     * @param int $firstTeamId
     * @return GameDTO
     * @throws WrongTeamException
     */
    public function setFirstTeamId(int $firstTeamId): GameDTO
    {
        if ($firstTeamId < 0) {
            throw new WrongTeamException('Team ID can\'t be lower than zero');
        }

        $this->firstTeamId = $firstTeamId;
        return $this;
    }


    /**
     * @param int $secondTeamId
     * @return GameDTO
     * @throws WrongTeamException
     */
    public function setSecondTeamId(int $secondTeamId): GameDTO
    {
        if ($secondTeamId < 0) {
            throw new WrongTeamException('Team ID can\'t be lower than zero');
        }

        $this->secondTeamId = $secondTeamId;
        return $this;
    }


    /**
     * @param mixed $firstTeamScore
     * @return GameDTO
     */
    public function setFirstTeamScore(?int $firstTeamScore): GameDTO
    {
        $this->firstTeamScore = $firstTeamScore;
        return $this;
    }

    /**
     * @param mixed $secondTeamScore
     * @return GameDTO
     */
    public function setSecondTeamScore(?int $secondTeamScore): GameDTO
    {
        $this->secondTeamScore = $secondTeamScore;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWinner()
    {
        $this->winnerId = $this->firstTeamScore > $this->secondTeamScore ? $this->firstTeamId : $this->secondTeamId;

        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'first_team' => $this->firstTeamId,
            'second_team' => $this->secondTeamId,
            'first_team_score' => $this->firstTeamScore,
            'second_team_score' => $this->secondTeamScore,
            'winner_id' => $this->winnerId
        ];
    }

}

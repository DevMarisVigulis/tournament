<?php

namespace App\Generators;

use Illuminate\Support\Collection;

interface GeneratorInterface
{
    public function generate(Collection $teams);
}

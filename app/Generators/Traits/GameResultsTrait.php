<?php

namespace App\Generators\Traits;

use App\Models\DTO\GameDTO;
use App\Models\Exceptions\WrongTeamException;
use Exception;

trait GameResultsTrait
{
    /**
     * @param int $firstTeamId
     * @param int $secondTeamId
     * @return array
     * @throws Exception
     */
    private function play(int $firstTeamId, int $secondTeamId): array
    {
        try {
            $game = new GameDTO();

            [$firstTeamScore, $secondTeamScore] = $firstTeamId === $secondTeamId ? [null, null] : $this->generateGameScores();

            return $game->setFirstTeamId($firstTeamId)
                ->setSecondTeamId($secondTeamId)
                ->setFirstTeamScore($firstTeamScore)
                ->setSecondTeamScore($secondTeamScore)
                ->getWinner()
                ->toArray();

        } catch (WrongTeamException $exception) {
            //TODO: handle exception
        }
    }


    /**
     * @return array
     * @throws Exception
     */
    private function generateGameScores(): array
    {
        $firstTeamScore = random_int(0, 5);
        $secondTeamScore = random_int(0, 5);

        if ($firstTeamScore === $secondTeamScore) {
            return $this->generateGameScores();
        }

        return [$firstTeamScore, $secondTeamScore];
    }
}

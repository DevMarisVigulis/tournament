<?php

namespace App\Generators;

use App\Generators\Traits\GameResultsTrait;
use App\Models\Team;
use Exception;
use Illuminate\Support\{Arr, Collection};

class TournamentTableGenerator implements GeneratorInterface
{
    use GameResultsTrait;

    /**
     * @param Collection $teams
     * @return Collection
     * @throws Exception
     */
    public function generate(Collection $teams): Collection
    {
        $pairs = $this->generatePairs($teams);

        return $this->generateResults($pairs);
    }


    /**
     * @param Collection $teams
     * @return array
     */
    private function generatePairs(Collection $teams): array
    {
        $pairs = [];

        /** @var Team $team */
        $teams->each(static function ($team) use (&$pairs, $teams) {
            $pairs[$team->id] = $teams
                ->pluck('id')
                ->toArray();

        });

        return $pairs;
    }


    /**
     * @param array $pairs
     * @return Collection
     * @throws Exception
     */
    private function generateResults(array $pairs): Collection
    {
        $results = new Collection();

        foreach ($pairs as $teamId => $opponents) {
            foreach (Arr::flatten($opponents) as $key => $opponent) {
                $results->push($this->play($teamId, $opponent));
            }
        }
        return $results;
    }
}

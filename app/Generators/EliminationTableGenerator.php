<?php

namespace App\Generators;

use App\Generators\Traits\GameResultsTrait;
use App\Models\DTO\ResultDTO;
use App\Models\Exceptions\WrongTeamException;
use Exception;
use Illuminate\Support\{Arr, Collection};

class EliminationTableGenerator implements GeneratorInterface
{
    /**
     * @var Collection
     */
    private $rounds;

    use GameResultsTrait;

    /**
     * @param Collection $teams
     * @return Collection
     * @throws Exception
     */
    public function generate(Collection $teams): Collection
    {
        $pairs = $this->generateFirstPairs($teams);
        $this->generateResults($pairs);

        return $this->rounds;
    }

    /**
     * @param Collection $teams
     * @return Collection
     */
    private function generateFirstPairs(Collection $teams): Collection
    {
        return new Collection(
            array_combine(array_keys($teams->first()), array_keys($teams->last()))
        );
    }

    /**
     * Generate results step-by-step
     *
     * @param Collection $pairs
     * @throws Exception
     */
    private function generateResults(Collection $pairs): void
    {
        $firstElimination = $this->getFirstRoundResults($pairs);
        $secondElimination = $this->getNextRoundsResults($this->getWinners($firstElimination));
        $thirdElimination = $this->getNextRoundsResults($this->getWinners($secondElimination));
        $this->rounds = Collection::make([$firstElimination, $secondElimination, $thirdElimination]);
    }

    /**
     * @param Collection $pairs
     * @return Collection
     * @throws Exception
     */
    private function getFirstRoundResults(Collection $pairs): Collection
    {
        $results = new Collection();
        foreach ($pairs as $firstTeam => $secondTeam) {
            $results->push($this->play($firstTeam, $secondTeam));
        }
        return $results;
    }

    /**
     * @param int $firstTeamId
     * @param int $secondTeamId
     * @return array
     * @throws Exception
     */
    private function play(int $firstTeamId, int $secondTeamId): array
    {
        try {
            $result = new ResultDTO();

            [$firstTeamScore, $secondTeamScore] = $this->generateGameScores();

            return $result
                ->setFirstTeamId($firstTeamId)
                ->setSecondTeamId($secondTeamId)
                ->setFirstTeamScore($firstTeamScore)
                ->setSecondTeamScore($secondTeamScore)
                ->getWinner()
                ->toArray();


        } catch (WrongTeamException $exception) {
            //TODO: handle exception
        }
    }

    /**
     * @param Collection $teams
     * @return Collection
     * @throws Exception
     */
    private function getNextRoundsResults(Collection $teams): Collection
    {
        $results = new Collection();
        $pairs = $teams->split(2)->toArray();

        foreach ($pairs as $pair) {
            if ($this->winnerIsRevealed($pair)) {
                $pair = Arr::flatten($pairs);
                return $results->push($this->play($pair[0], $pair[1]));
            }
            $results->push($this->play($pair[0], $pair[1]));

        }
        return $results;
    }

    /**
     * @param array $pair
     * @return bool
     */
    private function winnerIsRevealed(array $pair): bool
    {
        return count($pair) < 2;
    }

    /**
     * @param Collection $roundResults
     * @return Collection
     */
    private function getWinners(Collection $roundResults): Collection
    {
        return $roundResults->pluck('winner_id');
    }
}

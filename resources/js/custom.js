$(document).ready(function(){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.generate-results-btn').on('click', function (e) {
        e.preventDefault();

        let leagueId = $(this).data('league-id');

        $.ajax({
            method: "POST",
            url: "/tournaments/generate",
            data: { leagueId: leagueId}
        })
        .done(function( response ) {
            location.reload()
        });
    })

    $('.generate-eliminations-btn').on('click', function (e) {
        e.preventDefault();

        $.ajax({
            method: "POST",
            url: "/tournaments/generate-eliminations",
        })
        .done(function( response ) {
            location.reload();
        });

    })

});

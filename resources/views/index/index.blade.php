@extends('layouts.main')

@section('content')
    <div class="row">
        @foreach($leagues as $league)
            <div class="col-sm-12 col-md-12 col-lg-6">
                <button class="btn btn-sm btn-info mb-2 generate-results-btn" data-league-id="{{ $league->id }}">
                    Generate
                </button>
                <table class="table table-bordered tournament-table">
                    <tr>
                        <td colspan="10" align="center">
                            {{ $league->name }}
                        </td>
                    </tr>

                    @include('index.partials.results-table', ['league' => $league])

                </table>
            </div>
        @endforeach
    </div>
    <hr>
    <div class="row">
        <div class="col-sm-12 col-lg-6">
            <button class="btn btn-sm btn-success mb-2 generate-eliminations-btn">Get results</button>
        </div>
    </div>
    <div class="row brackets-container">
        <div class="col-5">
            @if($eliminations->isEmpty() === false)
                @include('index.partials.tournament-brackets', ['eliminations' => $eliminations])
            @endif
        </div>
    </div>
@endsection

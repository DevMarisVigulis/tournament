<div class="tournament">
    <div>
        @foreach($eliminations->take(4) as $elimination)
            <div>
                <ul>
                    <li class="winner">{{ $elimination->winner->name }}</li>
                    <li>{{ $elimination->loser->name }}</li>
                </ul>
            </div>
        @endforeach
    </div>
    <div>
        @foreach($eliminations->splice(4,2) as $elimination)
            <div>
                <ul>
                    <li class="winner">{{ $elimination->winner->name }}</li>
                    <li>{{ $elimination->loser->name }}</li>
                </ul>
            </div>
        @endforeach
    </div>
    <div>
        <div>
            <ul>
                <li class="winner">{{ $eliminations->last()->winner->name }}</li>
                <li>{{ $eliminations->last()->loser->name }}</li>
            </ul>
        </div>
    </div>
    <div>
        <div>
            <ul>
                <li class="winner">{{ $eliminations->last()->winner->name }}</li>
            </ul>
        </div>
    </div>
</div>

@foreach($league->teams as $team)

    @if ($loop->first)
        <tr>
            <td>Teams</td>
            @foreach($league->teams as $leagueTeam)
                <td> {{ $leagueTeam->name }} </td>
            @endforeach
            <td>Score</td>
        </tr>
    @endif

    <tr>
        <td> {{ $team->name }} </td>

        @forelse($team->games as $game)
            @if($game->first_team_score === null || $game->second_team_score === null)
                <td class="empty-cell"></td>
            @else
                <td class="results-cell">{{ $game->result }}</td>
            @endif
        @empty
            @for($counter = 0; $counter <= \App\Models\League::TEAMS_PER_LEAGUE; $counter++)
                <td class="results-cell"></td>
            @endfor
        @endforelse
        <td>{{ $team->score === 0 ? '-' : $team->score }}</td>
    </tr>

@endforeach

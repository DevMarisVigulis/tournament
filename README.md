**Instalācija**

 1. `composer install`
 2. `npm install && npm run dev`
 3. `cp .env.example .env`
 4. `.env` failā ierekstīt DB paramatrus
 5.  `php artisan migrate --seed`

**Uzdevums**

Sagatavot un aizpildīt turnīra grafiku (tabulu), kur komandas tiek sadalītas divās divīzijās A un B. Katrā divīzijā komandas izspēlē katra ar katru, kur gala rezultātā 4 labākās komandas, no katras divīzijas, nonāk izslēgšanas spēlēs. Izslēgšanas spēles grafiks ir pēc “egles” principa, (labākā komanda spēlē pret vājāko komandu), kur vinnētājs iet tālāk, bet zaudētājs izkrīt no turpmākās dalības. Rezultātā vinnētāja komanda ir tā, kas uzvar visas spēles izslēgšanas grafikā.

Papildus nosacījumi uzdevuma izpildei:

 - Uzdevumu izpildīt uz PHP, lai tas būtu savienojams ar versiju PHP7.*
 - Uzdevumā LŪGUMS parādīt savas labākas (!) zināšanas par OOP.
 - Lai komandas un to rezultāti nebūtu jāvada ar roku, ieteicams izmantot auto-ģenerēšanas principu, piemēram, uzģenerēt (nospiežot pogu) divīzijas A tabulu ar rezultātiem, tad otru divīzijas tabulu. Arī ar pogu tiek uzģenerēta trešā (izslēgšanas) tabula ar turnīra rezultātiem.
 - UI izkārtojums pēc brīvas izvēles, bet velamies redzēt MVC
- Rezultātiem ir jābūt saglabātiem db (MySQL/Mariadb/PostgreSQL)
- Kodu var pieglabāt GitHub vai Bitbucket kopā ar komentāriem un instrukcijām

Pēc brīvas izvēles var izmantot vai neizmantot kādu no populārajiem PHP ietvariem (framwork) - Laravel, Zend, Symfony vai komponentēm.
